package com.lamrani.musicapplication.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lamrani.musicapplication.Adapter.UserAlbumsAdapter;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.presenter.UserAlbumsPresenter;
import com.lamrani.musicapplication.ui.fragments.FragmentSearch;
import com.lamrani.musicapplication.view.IUserAlbumsView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IUserAlbumsView {

    RecyclerView recyclerView;
    UserAlbumsAdapter adapter;
    UserAlbumsPresenter presenter;
    ProgressDialog progressDoalog;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    hideFragments();
                    return true;
                case R.id.navigation_search:
                    hideFragments();
                    showFragment(new FragmentSearch());
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Initialize the view
        initView();

        // Configure the loading view
        progressDoalog = Utility.configProgressDialog(this);

        // The presenter
        presenter = new UserAlbumsPresenter(this, this);

        // show results
        presenter.onLoadUserAlbums();
    }

    private void initView() {
        // initialize the recyclerView
        init_recycler_view();
    }

    private void init_recycler_view() {
        recyclerView = findViewById(R.id.recyclerView_user_albums);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        adapter = new UserAlbumsAdapter(this);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void showProgress() {
        progressDoalog.show();
    }

    @Override
    public void hideProgress() {
        progressDoalog.dismiss();
    }

    @Override
    public void showMessage(int string){
        recyclerView.setVisibility(View.GONE);
        findViewById(R.id.container_message).setVisibility(View.VISIBLE);
        TextView tv_message = findViewById(R.id.tv_message);
        tv_message.setText(getString(string));
    }

    @Override
    public void hideMessage(){
        recyclerView.setVisibility(View.VISIBLE);
        findViewById(R.id.container_message).setVisibility(View.GONE);
    }

    @Override
    public void onLoadAlbumsSuccess(List<AlbumDetail> albums) {
        adapter.addAll(albums);
        hideMessage();
    }

    @Override
    public void onLoadAlbumsError(int message) {
        showMessage(message);
    }

    @Override
    public void onRefreshList() {
        adapter.removeAllItems();
        presenter.onLoadUserAlbums();
    }


    @Override
    public void onBackPressed() {
        setTitle(R.string.my_albums);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    /**
     * Show fragment
     */
    public void showFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_frame, fragment, fragment.getClass().getName());
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    /**
     * Hide all fragments
     */
    public void hideFragments(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }
        setTitle(R.string.my_albums);
    }

}
