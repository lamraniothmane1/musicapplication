package com.lamrani.musicapplication.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.Adapter.ArtistsAdapter;
import com.lamrani.musicapplication.helper.EndlessRecyclerViewScrollListener;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.Artist;
import com.lamrani.musicapplication.presenter.ArtistsPresenter;
import com.lamrani.musicapplication.ui.MainActivity;
import com.lamrani.musicapplication.view.IArtistsView;
import java.util.List;



public class FragmentSearch extends Fragment implements IArtistsView{

    private View view;
    private MainActivity activity;
    RecyclerView recyclerView;
    EditText et_search;
    ArtistsAdapter adapter;
    ArtistsPresenter presenter;
    ProgressDialog progressDoalog;
    // Endless recycler view pagination loader
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search_artists, container, false);
        activity = (MainActivity) getActivity();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // Configure the loading view
        progressDoalog = Utility.configProgressDialog(activity);

        // The presenter
        presenter = new ArtistsPresenter(activity, this);

        // initialize the recyclerView
        init_recycler_view();

        // Initialize the search view input and button listener
        initSearchView();

    }


    /**
     * Initialize the search view with listeners
     */
    private void initSearchView() {
        et_search = view.findViewById(R.id.et_search);
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                String search_query = textView.getText().toString();

                // load Artists
                presenter.onSearchArtists(search_query, 1);
                return false;
            }
        });

        activity.findViewById(R.id.btn_search_artist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // hide keyboard
                Utility.hideKeyboard(activity);
                // get the query
                String search_query = et_search.getText().toString();

                // load Artists
                presenter.onSearchArtists(search_query, 1);
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(R.string.search_artist_title);
    }

    private void init_recycler_view() {
        recyclerView = view.findViewById(R.id.recyclerView_artists);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);

        adapter = new ArtistsAdapter((MainActivity) activity);
        recyclerView.setAdapter(adapter);

        // set the pagination loading
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list

                presenter.onSearchArtists(et_search.getText().toString(), page+1);
            }

        };

        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

    }


    @Override
    public void showProgress() {
        progressDoalog.show();
    }

    @Override
    public void hideProgress() {
        progressDoalog.dismiss();
    }

    @Override
    public void showMessage(int string){
        recyclerView.setVisibility(View.GONE);
        view.findViewById(R.id.container_message).setVisibility(View.VISIBLE);
        TextView tv_message = view.findViewById(R.id.tv_message);
        tv_message.setText(string);
    }

    @Override
    public void hideMessage(){
        recyclerView.setVisibility(View.VISIBLE);
        activity.findViewById(R.id.container_message).setVisibility(View.GONE);
    }

    @Override
    public void onCallArtistsSuccess(List<Artist> artists) {
        adapter.addAll(artists);
    }

    @Override
    public void removeAllItems() {
        adapter.removeAllItems();
    }

    @Override
    public void onCallArtistsError(int message) {
        showMessage(message);
        hideProgress();
    }

}
