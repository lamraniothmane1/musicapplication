package com.lamrani.musicapplication.ui.fragments;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lamrani.musicapplication.Adapter.TracksAdapter;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.model.Track;
import com.lamrani.musicapplication.presenter.AlbumDetailsPresenter;
import com.lamrani.musicapplication.ui.MainActivity;
import com.lamrani.musicapplication.view.IAlbumDetailsView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;


public class FragmentAlbumDetails extends Fragment implements IAlbumDetailsView{

    private View view;
    protected MainActivity activity;
    RecyclerView recyclerView;
    TracksAdapter adapter;
    AlbumDetailsPresenter presenter;
    ProgressDialog progressDoalog;
    String album_name, artist_name;
    private AlbumDetail albumDetail;
    Button btnAdd;
    ImageView iv_album;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_album_details, container, false);
        activity = (MainActivity) getActivity();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            album_name = getArguments().getString(Utility.ALBUM);
            artist_name = getArguments().getString(Utility.ARTIST);
            albumDetail = (AlbumDetail) getArguments().getSerializable(Utility.ALBUM_DETAIL);
        }

        // Configure the loading view
        progressDoalog = Utility.configProgressDialog(activity);

        // The presenter
        presenter = new AlbumDetailsPresenter(activity, this);

        // Initialize the view
        initView();

        // set album info
        setAlbumInfo();
    }

    private void initView() {
        // initialize the recyclerView
        init_recycler_view();

        // btn add/remove listener
        btnAdd = view.findViewById(R.id.btn_add_album);
        iv_album = view.findViewById(R.id.iv_image);
    }

    private void setAlbumInfo() {
        // Set the album name
        TextView tv_album = view.findViewById(R.id.tv_name);
        tv_album.setText(album_name);
        // Set the artist name
        TextView tv_artist = view.findViewById(R.id.tv_artist);
        tv_artist.setText(artist_name);

        // If the album detail is set then this fragment is started from the Main activity (User Albums click)
        if(albumDetail != null){
            if(Utility.isNetworkAvailable(activity)){
                // get result from online
                presenter.onLoadAlbumDetails(artist_name, album_name);
            }
            else{
                // get result from offline
                onLoadAlbumDetailsFromDatabse(albumDetail);
            }
        }
        else{
            // show results from server
            presenter.onLoadAlbumDetails(artist_name, album_name);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(R.string.album_details);
    }

    private void init_recycler_view() {
        recyclerView = view.findViewById(R.id.recyclerView_tracks);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));

        adapter = new TracksAdapter(activity);
        recyclerView.setAdapter(adapter);

    }


    @Override
    public void showProgress() {
        progressDoalog.show();
    }

    @Override
    public void hideProgress() {
        progressDoalog.dismiss();
    }

    @Override
    public void showMessage(int string){
        recyclerView.setVisibility(View.GONE);
        view.findViewById(R.id.container_message).setVisibility(View.VISIBLE);
        TextView tv_message = view.findViewById(R.id.tv_message);
        tv_message.setText(string);
    }


    @Override
    public void onCallAlbumDetailsSuccess(final AlbumDetail albumDetail) {

        this.albumDetail = albumDetail;
        Log.i("MBID success", albumDetail.getMbid() + "");

        final boolean album_exists = presenter.onCheckAlbumExists(albumDetail.getArtistName(), albumDetail.getName());
        if(album_exists){
            btnAdd.setText(R.string.remove_album);
        }
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(presenter.onCheckAlbumExists(albumDetail.getArtistName(), albumDetail.getName())){
                    presenter.onRemoveAlbum(albumDetail);
                }
                else{
                    presenter.onAddAlbum(albumDetail);
                }
            }
        });

        // Setting the album details
        setValues(albumDetail);

        // setting the image
        String url_image = albumDetail.getImageUrl("large");
        if(url_image != null){
            if(!url_image.isEmpty()){

                Transformation transformation = new RoundedTransformationBuilder()
                        .borderColor(Color.GRAY)
                        .borderWidthDp(1)
                        .cornerRadiusDp(100)
                        .oval(false)
                        .build();

                Picasso.get()
                        .load(url_image)
                        .fit()
                        .transform(transformation)
                        .into(iv_album);
            }
        }
    }

    @Override
    public void onLoadAlbumDetailsFromDatabse(AlbumDetail albumDetail) {
        setValues(albumDetail);

        // Setting the image
        Bitmap bitmap_img = albumDetail.getImageBitmap();
        if(bitmap_img != null){
            //holder.imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap_img, 250, 100, false));

            // set the rounded image using RoundedBitmapDrawable (It doesnt contains the borders ..
            Resources res = activity.getResources();
            RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(res, bitmap_img);
            dr.setCircular(true);
            iv_album.setImageDrawable(dr);
        }
    }

    @Override
    public void onCallAlbumDetailsError(int message) {
        showMessage(message);
        hideProgress();
    }

    @Override
    public void onSaveAlbumSuccess() {
        Toast.makeText(activity, R.string.album_added_successfully, Toast.LENGTH_SHORT).show();
        btnAdd.setText(R.string.remove_album);
        activity.onRefreshList();
    }

    @Override
    public void onSaveAlbumFailure() {
        Toast.makeText(activity, R.string.adding_album_failed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveAlbumSuccess() {
        Toast.makeText(activity, R.string.album_removed_successfully, Toast.LENGTH_SHORT).show();
        btnAdd.setText(R.string.add_album);
        activity.onRefreshList();
    }

    @Override
    public void onRemoveAlbumFailure() {
        Toast.makeText(activity, R.string.removing_album_failed, Toast.LENGTH_SHORT).show();
    }


    public void setValues(AlbumDetail albumDetail){

        // set listeners
        TextView tv_listeners = view.findViewById(R.id.tv_listeners);
        tv_listeners.setText(String.format("%s %s",albumDetail.getListeners(), getString(R.string.listeners)));
        // set play count
        TextView tv_play_count = view.findViewById(R.id.tv_play_count);
        tv_play_count.setText(String.format("%s %s %s", getString(R.string.played), albumDetail.getPlaycount(), getString(R.string.times)));

        // add tracks
        List<Track> tracks = albumDetail.getTracks();
        if(tracks != null){
            if(tracks.isEmpty()){
                showMessage(R.string.empty_result);
            }
            else{
                adapter.addAll(tracks);
            }
        }
        else{
            showMessage(R.string.empty_result);
        }

    }
}
