package com.lamrani.musicapplication.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lamrani.musicapplication.Adapter.AlbumsAdapter;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.EndlessRecyclerViewScrollListener;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.Album;
import com.lamrani.musicapplication.presenter.AlbumsPresenter;
import com.lamrani.musicapplication.ui.MainActivity;
import com.lamrani.musicapplication.view.IAlbumsView;

import java.util.List;


public class FragmentAlbums extends Fragment implements IAlbumsView{

    private View view;
    private MainActivity activity;
    RecyclerView recyclerView;
    AlbumsAdapter adapter;
    AlbumsPresenter presenter;
    ProgressDialog progressDoalog;
    String artist_name;
    // Endless recycler view pagination loader
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search_albums, container, false);
        activity = (MainActivity) getActivity();

        if (getArguments() != null) {
            artist_name = getArguments().getString(Utility.ARTIST);
        }
        else artist_name = null;

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // Configure the loading view
        progressDoalog = Utility.configProgressDialog(activity);

        // The presenter
        presenter = new AlbumsPresenter(activity, this);

        // initialize the recyclerView
        init_recycler_view();

        // show results
        presenter.onSearchAlbum(artist_name, 1);

    }


    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(getString(R.string.albums_of) + " " + artist_name);
    }

    private void init_recycler_view() {
        recyclerView = view.findViewById(R.id.recyclerView_albums_search);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new AlbumsAdapter(activity);
        recyclerView.setAdapter(adapter);

        // set the pagination loading
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list

                presenter.onSearchAlbum(artist_name, page+1);
            }

        };

        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

    }


    @Override
    public void showProgress() {
        progressDoalog.show();
    }

    @Override
    public void hideProgress() {
        progressDoalog.dismiss();
    }

    public void showMessage(int string){
        recyclerView.setVisibility(View.GONE);
        view.findViewById(R.id.container_message).setVisibility(View.VISIBLE);
        TextView tv_message = view.findViewById(R.id.tv_message);
        tv_message.setText(string);
    }

    public void hideMessage(){
        recyclerView.setVisibility(View.VISIBLE);
        activity.findViewById(R.id.container_message).setVisibility(View.GONE);
    }

    @Override
    public void onCallAlbumsSuccess(List<Album> albums) {
        adapter.addAll(albums);
    }

    @Override
    public void onCallAlbumsError(int message) {
        showMessage(message);
        hideProgress();
    }

}
