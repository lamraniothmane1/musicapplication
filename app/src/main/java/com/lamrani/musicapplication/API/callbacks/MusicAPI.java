package com.lamrani.musicapplication.API.callbacks;


import com.lamrani.musicapplication.API.models.AlbumDetailsResponse;
import com.lamrani.musicapplication.API.models.AlbumResponse;
import com.lamrani.musicapplication.API.models.ArtistResponse;
import com.lamrani.musicapplication.model.AlbumDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Lamrani on 21/09/2017.
 */

public interface MusicAPI {

    @GET("?method=artist.search&format=json")
    Call<ArtistResponse> searchArtist(@Query("artist") String artist, @Query("page") int page, @Query("api_key") String key);

    @GET("?method=artist.gettopalbums&format=json")
    Call<AlbumResponse> searchAlbums(@Query("artist") String artist, @Query("page") int page, @Query("api_key") String key);

    @GET("?method=album.getinfo&format=json")
    Call<AlbumDetailsResponse> getAlbumDetails(@Query("artist") String artist, @Query("album") String album, @Query("api_key") String key);

}
