package com.lamrani.musicapplication.API.models;

import com.google.gson.annotations.SerializedName;
import com.lamrani.musicapplication.model.Album;

import java.util.ArrayList;
import java.util.List;

public class AlbumResponse {

    @SerializedName("topalbums")
    private TopAlbums topAlbums;

    public List<Album> getAlbums(){
        return topAlbums.albums;
    }

    private class TopAlbums {
        @SerializedName("album")
        private ArrayList<Album> albums;
    }
}
