package com.lamrani.musicapplication.API.models;

import com.google.gson.annotations.SerializedName;
import com.lamrani.musicapplication.model.Artist;

import java.util.ArrayList;
import java.util.List;

public class ArtistResponse {

    @SerializedName("results")
    private Results results;

    public Results getResults() {
        return results;
    }

    public List<Artist> getArtists(){
        Results results = getResults();
        if(results != null){
            ArtistMatches artistMatches = results.getArtistmatches();
            if(artistMatches != null){
                return artistMatches.getArtists();
            }
        }
        return null;
    }

    private class Results{
        @SerializedName("artistmatches")
        private ArtistMatches artistmatches;

        public ArtistMatches getArtistmatches() {
            return artistmatches;
        }
    }

    private class ArtistMatches{
        @SerializedName("artist")
        private ArrayList<Artist> artists;

        public ArrayList<Artist> getArtists() {
            return artists;
        }
    }



}
