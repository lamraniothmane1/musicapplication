package com.lamrani.musicapplication.API.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lamrani.musicapplication.model.AlbumDetail;

public class AlbumDetailsResponse {

    @SerializedName("album")
    private AlbumDetail albumDetail;

    public AlbumDetail getAlbumDetail() {
        return albumDetail;
    }
}
