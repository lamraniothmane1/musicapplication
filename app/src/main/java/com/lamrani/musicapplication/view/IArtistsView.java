package com.lamrani.musicapplication.view;

import com.lamrani.musicapplication.model.Artist;

import java.util.List;

public interface IArtistsView {
    void showProgress();
    void hideProgress();
    void showMessage(int message);
    void hideMessage();
    void onCallArtistsSuccess(List<Artist> artists);
    void removeAllItems();
    void onCallArtistsError(int message);
}
