package com.lamrani.musicapplication.view;

import com.lamrani.musicapplication.model.Album;
import com.lamrani.musicapplication.model.AlbumDetail;

import java.util.List;

public interface IAlbumDetailsView {
    void showProgress();
    void hideProgress();
    void showMessage(int message);
    void onCallAlbumDetailsSuccess(AlbumDetail albumDetail);
    void onLoadAlbumDetailsFromDatabse(AlbumDetail albumDetail);
    void onCallAlbumDetailsError(int message);
    void onSaveAlbumSuccess();
    void onSaveAlbumFailure();
    void onRemoveAlbumSuccess();
    void onRemoveAlbumFailure();
}
