package com.lamrani.musicapplication.view;

import com.lamrani.musicapplication.model.Album;
import com.lamrani.musicapplication.model.Artist;

import java.util.List;

public interface IAlbumsView {
    void showProgress();
    void hideProgress();
    void showMessage(int message);
    void hideMessage();
    void onCallAlbumsSuccess(List<Album> albums);
    void onCallAlbumsError(int message);
}
