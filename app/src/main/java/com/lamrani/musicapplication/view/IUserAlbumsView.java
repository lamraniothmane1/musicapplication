package com.lamrani.musicapplication.view;

import com.lamrani.musicapplication.model.AlbumDetail;

import java.util.List;

public interface IUserAlbumsView {
    void showProgress();
    void hideProgress();
    void showMessage(int message);
    void hideMessage();
    void onLoadAlbumsSuccess(List<AlbumDetail> albums);
    void onLoadAlbumsError(int message);
    void onRefreshList();
}
