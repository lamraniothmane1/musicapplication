package com.lamrani.musicapplication.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.model.AlbumDetail;

import java.io.ByteArrayOutputStream;


public class Utility {

    public static final String API_KEY = "415ef28a5f1e28ce1600bf0a39a07782";
    public static final String ARTIST = "artist";
    public static final String ALBUM = "album";
    public static final String ALBUM_DETAIL = "album_detail";
    public static final String ALBUM_NAME_COLUMN = "albumname";
    public static final String NAME_COLUMN = "name";
    public static final String ARTIST_NAME_COLUMN= "artistname";
    public static final String ARTIST_COLUMN= "artist";


    /**
     * Configure the progress dialog for loading
     */
    public static ProgressDialog configProgressDialog(Activity activity) {
        ProgressDialog progressDoalog = new ProgressDialog(activity);
        progressDoalog.setMessage(activity.getString(R.string.loading));
        progressDoalog.setTitle(activity.getString(R.string.app_name));
        progressDoalog.setCancelable(false);

        return progressDoalog;
    }


    /**
     * Hide keyboard
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Convert bitmap to byte array
     */
    public static byte[] getByteArray(Bitmap bitmap){
        if(bitmap != null){
            ByteArrayOutputStream blob = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /* Ignored for PNGs */, blob);
            byte[] bitmapdata = blob.toByteArray();
            return bitmapdata;
        }
        return null;
    }

    public static Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "img", null);
        return Uri.parse(path);
    }

    public static String withSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f %c",
                count / Math.pow(1000, exp),
                "kMGTPE".charAt(exp-1));
    }


}
