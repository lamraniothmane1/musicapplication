package com.lamrani.musicapplication.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.ui.MainActivity;
import com.lamrani.musicapplication.ui.fragments.FragmentAlbumDetails;

import java.util.ArrayList;
import java.util.List;


public class UserAlbumsAdapter extends RecyclerView.Adapter<UserAlbumsAdapter.Holder> {

    private List<AlbumDetail> items_data;
    private MainActivity activity;

    public UserAlbumsAdapter(MainActivity activity) {
        items_data = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public UserAlbumsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album, parent, false);
        return new Holder(row);
    }


    @Override
    public void onBindViewHolder(@NonNull UserAlbumsAdapter.Holder holder, int position) {
        AlbumDetail album = getItem(position);
        String label = album.getName();

        holder.tv_label.setText(label);

        // Setting the image
        Bitmap bitmap_img = album.getImageBitmap();
        if(bitmap_img != null){
            //holder.imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap_img, 250, 100, false));
            // set the rounded image using RoundedBitmapDrawable (It doesnt contains the borders ..
            Resources res = activity.getResources();
            RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(res, bitmap_img);
            dr.setCircular(true);
            holder.imageView.setImageDrawable(dr);
        }



    }

    @Override
    public int getItemCount() {
        return items_data.size();
    }

    /**
     * Add an item to the list
     * @param album
     */
    public void addItem(AlbumDetail album){
        items_data.add(album);
        notifyDataSetChanged();
    }

    public void addAll(List<AlbumDetail> albums){
        items_data.addAll(albums);
        notifyDataSetChanged();
    }

    /**
     * Clear the list
     */
    public void removeAllItems(){
        items_data.clear();
        notifyDataSetChanged();
    }

    /**
     * get an item at position
     */
    public AlbumDetail getItem(int position){
        return items_data.get(position);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Context context;
        private TextView tv_label;
        private ImageView imageView;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            context = itemView.getContext();

            tv_label = itemView.findViewById(R.id.tv_label);
            imageView = itemView.findViewById(R.id.iv_image);
        }

        @Override
        public void onClick(View view) {

            AlbumDetail selected_item = getItem(getAdapterPosition());

            FragmentAlbumDetails fragmentAlbumDetails = new FragmentAlbumDetails();
            // set the name of the artist as parameter to the next fragment
            Bundle bundle = new Bundle();
            bundle.putString(Utility.ALBUM, selected_item.getName());
            bundle.putString(Utility.ARTIST, selected_item.getArtistName());
            bundle.putSerializable(Utility.ALBUM_DETAIL, selected_item);
            fragmentAlbumDetails.setArguments(bundle);

            Utility.hideKeyboard(activity);

            // show the albums fragment
            activity.showFragment(fragmentAlbumDetails);

        }
    }

}
