package com.lamrani.musicapplication.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.Album;
import com.lamrani.musicapplication.ui.MainActivity;
import com.lamrani.musicapplication.ui.fragments.FragmentAlbumDetails;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;


public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.Holder> {

    private List<Album> items_data;
    private MainActivity activity;

    public AlbumsAdapter(MainActivity activity) {
        items_data = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public AlbumsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album, parent, false);
        return new Holder(row);
    }


    @Override
    public void onBindViewHolder(@NonNull AlbumsAdapter.Holder holder, int position) {
        Album album = getItem(position);
        String label = album.getName();

        holder.tv_label.setText(label);

       String url_image = album.getImageUrl("large");
       if(url_image != null){
           if(!url_image.isEmpty()){

               Transformation transformation = new RoundedTransformationBuilder()
                       .borderColor(Color.GRAY)
                       .borderWidthDp(1)
                       .cornerRadiusDp(30)
                       .oval(false)
                       .build();

               Picasso.get()
                       .load(url_image)
                       .fit()
                       .error(R.drawable.ic_music)
                       .transform(transformation)
                       .into(holder.imageView);
           }
       }




    }

    @Override
    public int getItemCount() {
        return items_data.size();
    }

    /**
     * Add an item to the list
     * @param album
     */
    public void addItem(Album album){
        items_data.add(album);
        notifyDataSetChanged();
    }

    public void addAll(List<Album> album){
        items_data.addAll(album);
        notifyDataSetChanged();
    }

    /**
     * Clear the list
     */
    public void removeAllItems(){
        items_data.clear();
        notifyDataSetChanged();
    }

    /**
     * get an item at position
     */
    public Album getItem(int position){
        return items_data.get(position);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Context context;
        private TextView tv_label;
        private ImageView imageView;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            context = itemView.getContext();

            tv_label = itemView.findViewById(R.id.tv_label);
            imageView = itemView.findViewById(R.id.iv_image);
        }

        @Override
        public void onClick(View view) {

            Album selected_item = getItem(getAdapterPosition());

            FragmentAlbumDetails fragmentAlbumDetails = new FragmentAlbumDetails();
            // set the name of the artist as parameter to the next fragment
            Bundle bundle = new Bundle();
            bundle.putString(Utility.ALBUM, selected_item.getName());
            bundle.putString(Utility.ARTIST, selected_item.getArtist().getName());
            fragmentAlbumDetails.setArguments(bundle);

            Utility.hideKeyboard(activity);

            if(Utility.isNetworkAvailable(activity)){
                // show the album details fragment
                activity.showFragment(fragmentAlbumDetails);
            }
            else{
                Toast.makeText(context, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
            }

        }
    }

}
