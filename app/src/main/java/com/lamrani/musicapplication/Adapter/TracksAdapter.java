package com.lamrani.musicapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.model.Track;
import com.lamrani.musicapplication.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;


public class TracksAdapter extends RecyclerView.Adapter<TracksAdapter.Holder> {

    private List<Track> items_data;
    private MainActivity activity;

    public TracksAdapter(MainActivity activity) {
        items_data = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public TracksAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_track, parent, false);
        return new Holder(row);
    }


    @Override
    public void onBindViewHolder(@NonNull TracksAdapter.Holder holder, int position) {
        Track track = getItem(position);
        String label = track.getName();

        holder.tv_label.setText(label);
        holder.tv_duration.setText(track.getDuration());

    }

    @Override
    public int getItemCount() {
        return items_data.size();
    }

    /**
     * Add an item to the list
     * @param track
     */
    public void addItem(Track track){
        items_data.add(track);
        notifyDataSetChanged();
    }

    public void addAll(List<Track> tracks){
        items_data.addAll(tracks);
        notifyDataSetChanged();
    }

    /**
     * Clear the list
     */
    public void removeAllItems(){
        items_data.clear();
        notifyDataSetChanged();
    }

    /**
     * get an item at position
     */
    public Track getItem(int position){
        return items_data.get(position);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Context context;
        private TextView tv_label, tv_duration;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            context = itemView.getContext();

            tv_label = itemView.findViewById(R.id.tv_label);
            tv_duration = itemView.findViewById(R.id.tv_duration);
        }

        @Override
        public void onClick(View view) {

            Track selected_item = getItem(getAdapterPosition());

            Toast.makeText(context, selected_item.getName(), Toast.LENGTH_SHORT).show();



        }
    }

}
