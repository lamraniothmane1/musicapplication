package com.lamrani.musicapplication.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.Artist;
import com.lamrani.musicapplication.ui.MainActivity;
import com.lamrani.musicapplication.ui.fragments.FragmentAlbums;

import java.util.ArrayList;
import java.util.List;


public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.Holder> {

    private List<Artist> items_data;
    private MainActivity activity;

    public ArtistsAdapter(MainActivity activity) {
        items_data = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public ArtistsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artist, parent, false);
        return new Holder(row);
    }


    @Override
    public void onBindViewHolder(@NonNull ArtistsAdapter.Holder holder, int position) {
        Artist artist = getItem(position);
        String label = artist.getName();

        holder.tv_label.setText(label);

    }

    @Override
    public int getItemCount() {
        return items_data.size();
    }

    /**
     * Add an item to the list
     * @param artist
     */
    public void addItem(Artist artist){
        items_data.add(artist);
        notifyDataSetChanged();
    }

    public void addAll(List<Artist> artists){
        items_data.addAll(artists);
        notifyDataSetChanged();
    }

    /**
     * Clear the list
     */
    public void removeAllItems(){
        items_data.clear();
        notifyDataSetChanged();
    }

    /**
     * get an item at position
     */
    public Artist getItem(int position){
        return items_data.get(position);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Context context;
        private TextView tv_label;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            context = itemView.getContext();

            tv_label = itemView.findViewById(R.id.tv_label);
        }

        @Override
        public void onClick(View view) {

            Artist selected_item = getItem(getAdapterPosition());

            FragmentAlbums fragmentAlbums = new FragmentAlbums();
            // set the name of the artist as parameter to the next fragment
            Bundle bundle = new Bundle();
            bundle.putString(Utility.ARTIST, selected_item.getName());
            fragmentAlbums.setArguments(bundle);

            Utility.hideKeyboard(activity);

            if(Utility.isNetworkAvailable(activity)){
                // show the albums fragment
                activity.showFragment(fragmentAlbums);
            }
            else{
                Toast.makeText(context, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
            }



        }
    }

}
