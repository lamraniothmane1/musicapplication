package com.lamrani.musicapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lamrani.musicapplication.API.models.ImageResponse;

import java.util.ArrayList;


public class Album {

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private ArrayList<ImageResponse> image_all_sizes;

    @SerializedName("playcount")
    private long playcount;

    @SerializedName("artist")
    private Artist artist;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPlaycount() {
        return playcount;
    }

    public void setPlaycount(long playcount) {
        this.playcount = playcount;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public String getImageUrl(String size) {
        for (ImageResponse imageResponse : image_all_sizes) {
            if(imageResponse.getSize().equals(size)){
                return imageResponse.getUrl();
            }
        }
        return null;
    }
}
