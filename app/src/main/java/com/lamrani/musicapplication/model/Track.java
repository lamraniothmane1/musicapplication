package com.lamrani.musicapplication.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Column;

public class Track extends SugarRecord {

    @SerializedName("name")
    private String name;

    @SerializedName("duration")
    private String duration_s;

    private String artistname;

    private String albumname;

    public Track(){

    }

    public String getName() {
        return name;
    }

    public String getDuration() {

        int duration = Integer.valueOf(duration_s);

        int s = duration%60;

        int minute = duration/60;

        int m = minute%60;

        int h = minute/60;

        if(h == 0){
            return String.format("%02d:%02d", m, s);
        }
        return String.format("%02d:%02d:%02d", h, m, s);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration_s() {
        return duration_s;
    }

    public void setDuration_s(String duration_s) {
        this.duration_s = duration_s;
    }

    public String getArtist() {
        return artistname;
    }

    public void setArtist(String artist) {
        this.artistname = artist;
    }

    public String getAlbum() {
        return albumname;
    }

    public void setAlbum(String album) {
        this.albumname = album;
    }
}
