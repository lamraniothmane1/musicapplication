package com.lamrani.musicapplication.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.google.gson.annotations.SerializedName;
import com.lamrani.musicapplication.API.models.ImageResponse;
import com.lamrani.musicapplication.helper.Utility;
import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Table
public class AlbumDetail extends SugarRecord implements Serializable {

    @SerializedName("name")
    private String name;

    @SerializedName("mbid")
    private String mbid;

    @SerializedName("image")
    private ArrayList<ImageResponse> image_all_sizes;

    @SerializedName("playcount")
    private String playcount;

    @SerializedName("listeners")
    private String listeners;

    @SerializedName("tracks")
    private TrackResponse tracks;

    @SerializedName("artist")
    private String artist;

    private byte[] imageByteArray;


    public AlbumDetail(){

    }


    public String getMbid() {
        return mbid;
    }

    public String getName() {
        return name;
    }

    public String getPlaycount() {
        return Utility.withSuffix(Long.valueOf(playcount));
    }

    public void setTracks(List<Track> tracks_list){
        this.tracks = new TrackResponse();
        this.tracks.tracks = (ArrayList<Track>) tracks_list;
    }


    public String getImageUrl(String size) {
        if(image_all_sizes != null){
            for (ImageResponse imageResponse : image_all_sizes) {
                if(imageResponse.getSize().equals(size)){
                    return imageResponse.getUrl();
                }
            }
        }
        return null;
    }

    public String getArtistName() {
        return artist;
    }

    public String getListeners() {
        return  Utility.withSuffix(Long.valueOf(listeners));
    }

    public ArrayList<Track> getTracks() {
        if(tracks != null) {
            return tracks.tracks;
        }
        return null;
    }

    public Bitmap getImageBitmap(){
        if(imageByteArray != null){
            return  BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);
        }
        return null;
    }

    public void setImage_byte_array(byte[] image_byte_array) {
        this.imageByteArray = image_byte_array;
    }

    public byte[] getImageByteArray() {
        return imageByteArray;
    }

    public void setImageByteArray(byte[] imageByteArray) {
        this.imageByteArray = imageByteArray;
    }

    private class TrackResponse {
        @SerializedName("track")
        private ArrayList<Track> tracks;
    }




}
