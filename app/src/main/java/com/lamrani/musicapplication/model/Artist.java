package com.lamrani.musicapplication.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Artist {

    @SerializedName("mbid")
    private String mbid;

    @SerializedName("name")
    private String name;

    @SerializedName("listeners")
    private long listeners;

    public String getMbid() {
        return mbid;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getListeners() {
        return listeners;
    }

    public void setListeners(long listeners) {
        this.listeners = listeners;
    }
}
