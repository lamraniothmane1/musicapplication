package com.lamrani.musicapplication.presenter;

import android.content.Context;
import android.util.Log;
import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.AlbumDetailsResponse;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.model.Track;
import com.lamrani.musicapplication.view.IAlbumDetailsView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.lamrani.musicapplication.helper.Utility.ALBUM_NAME_COLUMN;
import static com.lamrani.musicapplication.helper.Utility.ARTIST_COLUMN;
import static com.lamrani.musicapplication.helper.Utility.ARTIST_NAME_COLUMN;
import static com.lamrani.musicapplication.helper.Utility.NAME_COLUMN;


public class AlbumDetailsPresenter implements IAlbumDetailsPresenter{

    private MusicAPI musicAPI;
    private IAlbumDetailsView albumDetailsView;
    private Context context;

    public AlbumDetailsPresenter(Context context, IAlbumDetailsView albumDetailsView){
        Retrofit retrofit = OperatingApiClient.getClient();
        musicAPI =  retrofit.create(MusicAPI.class);
        this.albumDetailsView = albumDetailsView;
        this.context = context;
    }


    @Override
    public void onLoadAlbumDetails(String artist, String album) {
        if(!Utility.isNetworkAvailable(context)){
            albumDetailsView.showMessage(R.string.network_unavailable);
        }
        else{

            // show the progress dialog
            albumDetailsView.showProgress();

            // call the service
            Call<AlbumDetailsResponse> call_search = musicAPI.getAlbumDetails(artist, album, Utility.API_KEY);

            call_search.enqueue(new Callback<AlbumDetailsResponse>() {
                @Override
                public void onResponse(Call<AlbumDetailsResponse> call, Response<AlbumDetailsResponse> response) {
                    Log.w("AlbumDetails_call", "success");
                    Log.i("url", response.raw().request().url().toString());
                    // get the result
                    AlbumDetailsResponse result = response.body();
                    // extract the artist list
                    if(result != null){
                        AlbumDetail albumDetail = result.getAlbumDetail();
                        if(albumDetail != null){
                            // update view
                            albumDetailsView.onCallAlbumDetailsSuccess(albumDetail);
                        }
                        else albumDetailsView.showMessage(R.string.empty_result);
                    }
                    else{
                        albumDetailsView.showMessage(R.string.empty_result);
                    }

                    // dissmiss the progress dialog
                    albumDetailsView.hideProgress();
                }

                @Override
                public void onFailure(Call<AlbumDetailsResponse> call, Throwable t) {
                    Log.w("AlbumDetails_call", "failure");

                    albumDetailsView.onCallAlbumDetailsError(R.string.internal_error);
                }
            });
        }
    }

    @Override
    public boolean onCheckAlbumExists(String artist, String album) {
        if(artist != null && album != null){
            List<AlbumDetail> albumDetail =  AlbumDetail.find(AlbumDetail.class, ARTIST_COLUMN + " = ? and " + NAME_COLUMN + " = ?" , artist, album);

            if(albumDetail != null){
                return albumDetail.size() > 0;
            }
        }

        return false;
    }

    @Override
    public void onAddAlbum(AlbumDetail albumDetail) {
        if(albumDetail != null){
            try {
                AlbumDetail to_be_stored = new AddAlbumTask().execute(albumDetail).get();
                if(to_be_stored != null){
                    // saving the album
                    to_be_stored.save();
                    albumDetailsView.onSaveAlbumSuccess();
                }
                else{
                    albumDetailsView.onSaveAlbumFailure();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        else{
            albumDetailsView.onSaveAlbumFailure();
        }
    }

    @Override
    public void onRemoveAlbum(AlbumDetail albumDetail) {
        String album = albumDetail.getName();
        String artist  = albumDetail.getArtistName();
        List<AlbumDetail> ad =  AlbumDetail.find(AlbumDetail.class, ARTIST_COLUMN + " = ? and " + NAME_COLUMN + " = ?" , artist, album);
        List<Track> tracks =  Track.find(Track.class, ARTIST_NAME_COLUMN + " = ? and " + ALBUM_NAME_COLUMN + " = ?" , artist, album);

        if(ad != null){
            try{
                if(!ad.isEmpty()){
                    ad.get(0).delete();
                    for (Track track: tracks) {
                        track.delete();
                    }
                    albumDetailsView.onRemoveAlbumSuccess();
                }
                else{
                    albumDetailsView.onRemoveAlbumFailure();
                }
            }
            catch (Exception e){
                e.printStackTrace();
                albumDetailsView.onRemoveAlbumFailure();
            }
        }
        else {
            albumDetailsView.onRemoveAlbumFailure();
        }
    }





}
