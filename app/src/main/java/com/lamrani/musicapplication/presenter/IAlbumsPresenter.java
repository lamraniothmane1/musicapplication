package com.lamrani.musicapplication.presenter;

public interface IAlbumsPresenter {
    void onSearchAlbum(String artist, int page);
}
