package com.lamrani.musicapplication.presenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.model.Track;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class AddAlbumTask extends AsyncTask<AlbumDetail, Void, AlbumDetail> {
    @Override
    protected AlbumDetail doInBackground(AlbumDetail... albumDetails) {

        AlbumDetail albumDetail = albumDetails[0];

        Log.i("adding album, MBID" , "" + albumDetail.getMbid());
        // setting the tracks
        List<Track> tracks = albumDetail.getTracks();
        if(tracks != null){
            for (Track track : tracks) {
                track.setArtist(albumDetail.getArtistName());
                track.setAlbum(albumDetail.getName());
                track.save();
            }
        }

        try{
            // setting the image convert image to byte[]
            String image_url = albumDetail.getImageUrl("large");
            if(image_url != null){
                if (!image_url.isEmpty()){
                    Bitmap bitmap = null;
                    URL aURL = new URL(image_url);
                    URLConnection conn = aURL.openConnection();
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);
                    bitmap = BitmapFactory.decodeStream(bis);
                    bis.close();
                    is.close();

                    if(bitmap != null){
                        Log.i("Bitmap: ", "" + bitmap.getHeight());
                        byte[] byteArray = Utility.getByteArray(bitmap);
                        albumDetail.setImage_byte_array(byteArray);
                        Log.i("ArrayByte: ", "" + byteArray.length);
                    }
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return albumDetail;
    }

    @Override
    protected void onPostExecute(AlbumDetail albumDetail) {
        super.onPostExecute(albumDetail);
    }
}