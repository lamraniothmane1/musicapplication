package com.lamrani.musicapplication.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.AlbumResponse;
import com.lamrani.musicapplication.API.models.ArtistResponse;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.Album;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.model.Artist;
import com.lamrani.musicapplication.view.IAlbumsView;
import com.lamrani.musicapplication.view.IArtistsView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AlbumsPresenter implements IAlbumsPresenter{

    private MusicAPI musicAPI;
    private IAlbumsView albumsView;
    private Context context;

    public AlbumsPresenter(Context context, IAlbumsView albumsView){
        Retrofit retrofit = OperatingApiClient.getClient();
        musicAPI =  retrofit.create(MusicAPI.class);
        this.albumsView = albumsView;
        this.context = context;
    }


    @Override
    public void onSearchAlbum(String artist, int page) {
        if(artist == null){
            albumsView.showMessage(R.string.internal_error);
        }
        else{
            if(artist.isEmpty()){
                albumsView.showMessage(R.string.empty_result);
            }
            else{
                if(!Utility.isNetworkAvailable(context)){
                    albumsView.showMessage(R.string.network_unavailable);
                }
                else{

                    // show the progress dialog
                    albumsView.showProgress();

                    // call the service
                    Call<AlbumResponse> call_search = musicAPI.searchAlbums(artist, page, Utility.API_KEY);
                    call_search.enqueue(new Callback<AlbumResponse>() {
                        @Override
                        public void onResponse(Call<AlbumResponse> call, Response<AlbumResponse> response) {
                            Log.w("SearchAlbums_call", "success");
                            // get the result
                            AlbumResponse result = response.body();
                            // extract the artist list
                            if(result != null){
                                List<Album> albums = result.getAlbums();
                                if(result.getAlbums() != null){
                                    // add items to the adapter
                                    albumsView.onCallAlbumsSuccess(albums);

                                    // show just the list (without the message)
                                    albumsView.hideMessage();
                                }
                                else{
                                    albumsView.showMessage(R.string.empty_result);
                                }
                            }
                            else{
                                albumsView.showMessage(R.string.empty_result);
                            }

                            // dissmiss the progress dialog
                            albumsView.hideProgress();

                        }

                        @Override
                        public void onFailure(Call<AlbumResponse> call, Throwable t) {
                            Log.w("SearchAlbums_call", "failure");

                            albumsView.onCallAlbumsError(R.string.internal_error);
                        }
                    });
                }
            }
        }
    }
}
