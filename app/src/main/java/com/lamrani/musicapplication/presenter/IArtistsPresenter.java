package com.lamrani.musicapplication.presenter;

public interface IArtistsPresenter {
    void onSearchArtists(String artist, int page);
}
