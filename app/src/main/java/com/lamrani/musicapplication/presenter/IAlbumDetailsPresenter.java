package com.lamrani.musicapplication.presenter;

import com.lamrani.musicapplication.model.AlbumDetail;

public interface IAlbumDetailsPresenter {
    void onLoadAlbumDetails(String artist, String album);
    boolean onCheckAlbumExists(String artist, String album);
    void onAddAlbum(AlbumDetail albumDetail);
    void onRemoveAlbum(AlbumDetail albumDetail);
}
