package com.lamrani.musicapplication.presenter;

public interface IUserAlbumsPresenter {
    void onLoadUserAlbums();
}
