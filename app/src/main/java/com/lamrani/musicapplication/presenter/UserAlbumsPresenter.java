package com.lamrani.musicapplication.presenter;

import android.app.Activity;
import android.content.Context;

import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.model.Track;
import com.lamrani.musicapplication.view.IUserAlbumsView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.lamrani.musicapplication.helper.Utility.ALBUM_NAME_COLUMN;
import static com.lamrani.musicapplication.helper.Utility.ARTIST_NAME_COLUMN;

public class UserAlbumsPresenter implements IUserAlbumsPresenter{

    private IUserAlbumsView userAlbumsView;
    private Context context;

    public UserAlbumsPresenter(Activity context, IUserAlbumsView userAlbumsView){
        this.userAlbumsView = userAlbumsView;
        this.context = context;
    }

    @Override
    public void onLoadUserAlbums() {
        userAlbumsView.showProgress();
        try{
            Iterator<AlbumDetail> iterator = AlbumDetail.findAll(AlbumDetail.class);

            List<AlbumDetail> albumDetailList = new ArrayList<>();
            while (iterator.hasNext()){
                AlbumDetail albumDetail = iterator.next();

                // set the tracks
                List<Track> tracks = Track.find(Track.class, ARTIST_NAME_COLUMN + " = ? and " + ALBUM_NAME_COLUMN + " = ?", albumDetail.getArtistName(), albumDetail.getName());
                albumDetail.setTracks(tracks);

                albumDetailList.add(albumDetail);
            }

            if(albumDetailList.isEmpty()){
                userAlbumsView.onLoadAlbumsError(R.string.user_albums_empty);
            }
            else{
                userAlbumsView.onLoadAlbumsSuccess(albumDetailList);
            }
        }
        catch (Exception e){
            e.printStackTrace();
            userAlbumsView.onLoadAlbumsError(R.string.internal_error);
        }
        userAlbumsView.hideProgress();
    }


}
