package com.lamrani.musicapplication.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.ArtistResponse;
import com.lamrani.musicapplication.R;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.Artist;
import com.lamrani.musicapplication.view.IArtistsView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ArtistsPresenter implements IArtistsPresenter{

    private MusicAPI musicAPI;
    private IArtistsView artistsView;
    private Context context;

    public ArtistsPresenter(Context context, IArtistsView artistsView){
        Retrofit retrofit = OperatingApiClient.getClient();
        musicAPI =  retrofit.create(MusicAPI.class);
        this.artistsView = artistsView;
        this.context = context;
    }


    @Override
    public void onSearchArtists(String artist, int page) {
        if(artist == null){
            artistsView.showMessage(R.string.internal_error);
        }
        else{
            if(artist.isEmpty()){
                artistsView.showMessage(R.string.search_message);
            }
            else{
                if(!Utility.isNetworkAvailable(context)){
                    artistsView.showMessage(R.string.network_unavailable);
                }
                else{

                    if(page == 1)
                        // remove all items from the list
                        artistsView.removeAllItems();

                    // show the progress dialog
                    artistsView.showProgress();

                    // call the service
                    Call<ArtistResponse> call_search = musicAPI.searchArtist(artist, page, Utility.API_KEY);
                    call_search.enqueue(new Callback<ArtistResponse>() {
                        @Override
                        public void onResponse(Call<ArtistResponse> call, Response<ArtistResponse> response) {
                            Log.w("SearchArtists_call", "success");
                            // get the result
                            ArtistResponse result = response.body();
                            // extract the artist list
                            if(result != null){
                                if(result.getResults() != null){
                                    List<Artist> artists = result.getArtists();
                                    if(artists != null){
                                        if(artists.isEmpty()){
                                            artistsView.showMessage(R.string.empty_result);
                                        }
                                        else{
                                            // add items to the adapter
                                            artistsView.onCallArtistsSuccess(artists);
                                            // show just the list (without the message)
                                            artistsView.hideMessage();
                                        }
                                    }
                                }
                                else{
                                    artistsView.showMessage(R.string.internal_error);
                                }
                            }
                            else{
                                artistsView.showMessage(R.string.internal_error);
                            }

                            // dissmiss the progress dialog
                            artistsView.hideProgress();

                        }

                        @Override
                        public void onFailure(Call<ArtistResponse> call, Throwable t) {
                            Log.w("SearchArtists_call", "failure");

                            artistsView.onCallArtistsError(R.string.internal_error);
                        }
                    });
                }
            }
        }

    }
}
