package com.lamrani.musicapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.AlbumResponse;
import com.lamrani.musicapplication.API.models.ArtistResponse;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.presenter.AlbumsPresenter;
import com.lamrani.musicapplication.presenter.ArtistsPresenter;
import com.lamrani.musicapplication.view.IAlbumsView;
import com.lamrani.musicapplication.view.IArtistsView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import static junit.framework.Assert.assertTrue;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class AlbumPresenterTest {

    private AlbumsPresenter artistsPresenter;
    @Mock
    private IAlbumsView albumsView;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        artistsPresenter = new AlbumsPresenter(appContext, albumsView);
    }

    @Test
    public void onSearchArtists_EmptyArtistName_CallsSchowMessage(){
        artistsPresenter.onSearchAlbum("", 1);
        Mockito.verify(albumsView).showMessage(R.string.empty_result);
    }

    @Test
    public void onSearchArtists_NullArtistName_CallsSchowMessage(){
        artistsPresenter.onSearchAlbum(null, 1);
        Mockito.verify(albumsView).showMessage(R.string.internal_error);
    }


    @Test
    public void test_call_SearchAlbums() {

        Retrofit retrofit = OperatingApiClient.getClient();
        MusicAPI musicAPI =  retrofit.create(MusicAPI.class);

        Call<AlbumResponse> call = musicAPI.searchAlbums("Michael Jackson", 1, Utility.API_KEY);

        try {
            Response<AlbumResponse> response = call.execute();
            AlbumResponse album_response = response.body();

            assertTrue(response.isSuccessful() && !album_response.getAlbums().isEmpty());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
