package com.lamrani.musicapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.AlbumDetailsResponse;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.orm.SugarContext;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();



        assertEquals("com.lamrani.musicapplication", appContext.getPackageName());
    }

    @Test
    public void saveTest(){
        Retrofit retrofit = OperatingApiClient.getClient();
        MusicAPI musicAPI =  retrofit.create(MusicAPI.class);

        Call<AlbumDetailsResponse> call = musicAPI.getAlbumDetails("Cher", "Believe", Utility.API_KEY);

        try {
            Response<AlbumDetailsResponse> response = call.execute();
            AlbumDetailsResponse album_response = response.body();

            assertTrue(response.isSuccessful() && album_response.getAlbumDetail().getArtistName().equals("Cher"));

            // Context of the app under test.
            Context appContext = InstrumentationRegistry.getTargetContext();
            SugarContext.init(appContext);


            AlbumDetail albumDetail = album_response.getAlbumDetail();
            albumDetail.save();




        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
