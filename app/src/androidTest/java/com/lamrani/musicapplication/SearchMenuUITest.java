package com.lamrani.musicapplication;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
public class SearchMenuUITest {

    @Rule
    public ActivityTestRule<OldActivity> mainActivityTestRule = new ActivityTestRule<>(OldActivity.class);
    @Test
    public void buttonClick_goToSecondActivity() {
        onView((withId(R.id.btn_search))).perform(click());
        onView((withId(R.id.search_fragment))).check(matches(isDisplayed()));
    }

}



