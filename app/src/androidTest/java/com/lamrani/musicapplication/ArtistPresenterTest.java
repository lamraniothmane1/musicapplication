package com.lamrani.musicapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.AlbumDetailsResponse;
import com.lamrani.musicapplication.API.models.ArtistResponse;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;
import com.lamrani.musicapplication.model.Artist;
import com.lamrani.musicapplication.presenter.ArtistsPresenter;
import com.lamrani.musicapplication.view.IArtistsView;
import com.orm.SugarContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyListOf;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class ArtistPresenterTest {

    private ArtistsPresenter artistsPresenter;
    @Mock
    private IArtistsView artistsView;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        artistsPresenter = new ArtistsPresenter(appContext, artistsView);
    }

    @Test
    public void onSearchArtists_EmptyArtistName_CallsSchowMessage(){
        artistsPresenter.onSearchArtists("", 1);
        Mockito.verify(artistsView).showMessage(R.string.search_message);
    }

    @Test
    public void onSearchArtists_NullArtistName_CallsSchowMessage(){
        artistsPresenter.onSearchArtists(null, 1);
        Mockito.verify(artistsView).showMessage(R.string.internal_error);
    }

    @Test
    public void shouldFillAdapter() throws Exception {
        artistsPresenter.onSearchArtists("Eminem", 1);
        Mockito.verify(artistsView).showProgress();
        Mockito.verify(artistsView).removeAllItems();
        //Mockito.verify(artistsView).onCallArtistsSuccess(anyListOf(Artist.class));
    }

    @Test
    public void test_call_SearchArtists() {

        Retrofit retrofit = OperatingApiClient.getClient();
        MusicAPI musicAPI =  retrofit.create(MusicAPI.class);

        Call<ArtistResponse> call = musicAPI.searchArtist("Michael Jackson", 1, Utility.API_KEY);

        try {
            Response<ArtistResponse> response = call.execute();
            ArtistResponse artist_response = response.body();

            assertTrue(response.isSuccessful() && !artist_response.getArtists().isEmpty());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
