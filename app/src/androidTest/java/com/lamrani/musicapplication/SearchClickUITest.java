package com.lamrani.musicapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.ui.fragments.FragmentSearch;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
//@RunWith(RobolectricTestRunner.class)
public class SearchClickUITest {

    @Rule
    public ActivityTestRule<OldActivity> mainActivityTestRule = new ActivityTestRule<>(OldActivity.class);

    @Before
    public void setUp(){
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), OldActivity.class);
        mainActivityTestRule.launchActivity(intent);
    }


    @Test
    public void buttonClick_goToSecondActivity() throws InterruptedException {
        FragmentSearch fragmentSearch = new FragmentSearch();
        // set the name of the artist as parameter to the next fragment
        Bundle bundle = new Bundle();
        bundle.putString(Utility.ARTIST, "test");
        fragmentSearch.setArguments(bundle);

        mainActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.container_frame, fragmentSearch).commit();

        onView(withId(R.id.search_fragment)).check(matches(isDisplayed()));
    }




}



