package com.lamrani.musicapplication;

import android.test.InstrumentationTestRunner;

import com.lamrani.musicapplication.API.OperatingApiClient;
import com.lamrani.musicapplication.API.callbacks.MusicAPI;
import com.lamrani.musicapplication.API.models.AlbumDetailsResponse;
import com.lamrani.musicapplication.API.models.AlbumResponse;
import com.lamrani.musicapplication.API.models.ArtistResponse;
import com.lamrani.musicapplication.helper.Utility;
import com.lamrani.musicapplication.model.AlbumDetail;

import org.junit.Test;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import static junit.framework.TestCase.assertTrue;

public class APITest {

    @Test
    public void check_searchArtists_call() {

        Retrofit retrofit = OperatingApiClient.getClient();
        MusicAPI musicAPI =  retrofit.create(MusicAPI.class);

        Call<ArtistResponse> call = musicAPI.searchArtist("Michael Jackson", 1, Utility.API_KEY);

        try {
            Response<ArtistResponse> response = call.execute();
            ArtistResponse artist_response = response.body();

            assertTrue(response.isSuccessful() && !artist_response.getArtists().isEmpty());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void check_searchAlbums_call() {

        Retrofit retrofit = OperatingApiClient.getClient();
        MusicAPI musicAPI =  retrofit.create(MusicAPI.class);

        Call<AlbumResponse> call = musicAPI.searchAlbums("Michael Jackson", 1, Utility.API_KEY);

        try {
            Response<AlbumResponse> response = call.execute();
            AlbumResponse album_response = response.body();

            assertTrue(response.isSuccessful() && !album_response.getAlbums().isEmpty());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void check_searchAlbumDetails_call() {

        Retrofit retrofit = OperatingApiClient.getClient();
        MusicAPI musicAPI =  retrofit.create(MusicAPI.class);

        Call<AlbumDetailsResponse> call = musicAPI.getAlbumDetails("Cher", "Believe", Utility.API_KEY);

        try {
            Response<AlbumDetailsResponse> response = call.execute();
            AlbumDetailsResponse album_response = response.body();

            assertTrue(response.isSuccessful() && !album_response.getAlbumDetail().getArtistName().equals("Cher"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
